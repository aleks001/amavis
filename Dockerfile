FROM debian:bullseye

# We want to install most dependencies via dzil, but there are some exceptions
RUN apt-get update && apt-get install -y \
  # We obviously need perl, before usuing dzil
  perl \
  # We also need dzil itself
  libdist-zilla-perl \
  # dzil needs this, but doesn't depend directly on it
  cpanminus \
  # required to build Unix-Syslog
  make \
  # spamassassin has many undeclared, optional dependencies. Keeping
  # them in sync ourselves wouls be tiresome, so we use the debiann
  # package (which has correct dependencies) instead.
  spamassassin \
  # required to build ZMQ::LibZMQ3
  libzmq3-dev \
  # required to build Net::LibIDN2
  libidn2-dev \
  # dzil doesn't build this out-of-the-box
  libberkeleydb-perl \
  # dzil doesn't build this out-of-the-box
  libnet-server-perl

COPY . /code
RUN cd /code && dzil install

RUN addgroup --system amavis
RUN adduser --system --ingroup amavis --home /var/lib/amavis --disabled-login --disabled-password amavis

RUN install -d -m 700 -o amavis -g amavis /var/lib/amavis/tmp /var/lib/amavis/db
COPY conf/amavisd-docker.conf /etc/amavisd.conf

#USER amavis
ENTRYPOINT ["/usr/local/bin/amavisd", "foreground"]
